import games.gomoku.Gomoku;

public class Main {

    public static void main(String[] args) {
        final Gomoku game = new Gomoku() {{
            setupGame(2, 15);
        }};

        while (!game.isGameFinished()) {
            game.playAutomaticMove();
            game.printBoard();
        }
        game.printWinner();

    }
}
