package implementation.simple;

import interfaces.MinMax;
import interfaces.MinMax.*;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import static java.lang.Integer.MAX_VALUE;
import static java.lang.Integer.MIN_VALUE;

public class FirstMinMax implements MinMax<Player,
        Move<Player>,
        Board<Move<Player>>,
        Heuristic<Player, Move<Player>, Board<Move<Player>>>,
        Game<Player, Move<Player>, Board<Move<Player>>>> {

    private Game<Player, Move<Player>, Board<Move<Player>>> game;
    private Board<Move<Player>> boardCopy;

    private static Player getOponent(Player player, List<Player> playersList) {
        for (Player oponent : playersList) {
            if (player != oponent) {
                return oponent;
            }
        }
        throw new IllegalStateException("There is no opponent for player: " + player.getId());
    }

    private static boolean isMovePromising(final Move<Player> move, final Board<Move<Player>> board,
                                           final Heuristic<Player, Move<Player>, Board<Move<Player>>> heuristic) {
        return heuristic.hasNeighbour(move, board);
    }

    private static List<Move<Player>> getMoves(final Player player, final Board<Move<Player>> board,
                                               final Heuristic<Player, Move<Player>, Board<Move<Player>>> heuristic,
                                               final Game<Player, Move<Player>, Board<Move<Player>>> game) {
        final TreeSet<MoveValue> filteredMoveValues = new TreeSet<>();
        final boolean boardIsEmpty = board.getAllMovesImmutable().isEmpty();
        for (Move<Player> move : game.getAllAvailableMovesForPlayer(player, board)) {
            //here we filter moves that are clearly 'bad'
            if (boardIsEmpty || isMovePromising(move, board, heuristic)) {
                filteredMoveValues.add(new MoveValue(heuristic.calculateValueForMove(move, board), move));
            }
        }
        final List<Move<Player>> filteredSortedMoves = new ArrayList<>(filteredMoveValues.size());
        //todo here use depth, percent, max number etc.
        while (!filteredMoveValues.isEmpty()) {
            filteredSortedMoves.add(filteredMoveValues.pollLast().move);
        }
        return filteredSortedMoves;
    }

    private static Move<Player> anyWinningMove(final List<Move<Player>> allMoves, final Board<Move<Player>> boardCopy, final Game<Player, Move<Player>, Board<Move<Player>>> game) {
        for (final Move<Player> move : allMoves) {
            if (game.isWinningMove(move, boardCopy)) {
                return move;
            }
        }
        return null;
    }

    @Override
    public void initMinMax(Game<Player, Move<Player>, Board<Move<Player>>> game) {
        this.game = game;
        this.boardCopy = game.getBoard().deepClone();
    }

    @Override
    public Move<Player> getBestMove(Player player, Heuristic<Player, Move<Player>, Board<Move<Player>>> heuristic) {
        final List<Move<Player>> allMoves = getMoves(player, boardCopy, heuristic, game);
        if (allMoves.isEmpty()) {
            throw new IllegalStateException("There are no moves on board for player: " + player.getId());
        }
        //if we are at the start of the game we do not need to look deeper than 0
        final int maxDepth = boardCopy.getAllMovesImmutable().isEmpty() ? 0 : 2;
        return getMaxMove(allMoves, player, getOponent(player, game.getAllPlayersImmutable()), heuristic, maxDepth).move;
    }

    private MoveValue getMaxMove(final List<Move<Player>> allMoves, final Player player, final Player oponent, final Heuristic<Player, Move<Player>, Board<Move<Player>>> heuristic, int depth) {
        //we are checking if we won
        final Move<Player> winningMove = anyWinningMove(allMoves, boardCopy, game);
        if (winningMove != null) {
            return new MoveValue(MAX_VALUE - 1, winningMove);//we won!
        }

        Move<Player> bestMove = null;
        MoveValue currentMove;
        int bestMoveValue = MIN_VALUE;
        int currentMoveValue;
        if (depth > 0) {
            for (Move<Player> move : allMoves) {
                boardCopy.makeMove(move);
                currentMove = getMinMove(getMoves(oponent, boardCopy, heuristic, game), player, oponent, heuristic, depth - 1);
                if (currentMove.value > bestMoveValue) {
                    bestMoveValue = currentMove.value;
                    bestMove = move;
                }
                boardCopy.revertLastMove();
            }
        } else {
            for (Move<Player> move : allMoves) {
                currentMoveValue = heuristic.calculateValueForMove(move, boardCopy);
                if (currentMoveValue > bestMoveValue) {
                    bestMoveValue = currentMoveValue;
                    bestMove = move;
                }
            }
        }
        return new MoveValue(bestMoveValue, bestMove);
    }

    private MoveValue getMinMove(final List<Move<Player>> allMoves, final Player player, final Player oponent, final Heuristic<Player, Move<Player>, Board<Move<Player>>> heuristic, int depth) {
        //we are checking if we lost
        final Move<Player> winningMove = anyWinningMove(allMoves, boardCopy, game);
        if (winningMove != null) {
            return new MoveValue(MIN_VALUE + 1, winningMove);//we lost!
        }

        MoveValue worstMove = new MoveValue(MAX_VALUE, null);
        MoveValue currentMove;

        for (Move<Player> move : allMoves) {
            boardCopy.makeMove(move);
            currentMove = getMaxMove(getMoves(player, boardCopy, heuristic, game), player, oponent, heuristic, depth);
            if (currentMove.value < worstMove.value) {
                worstMove = currentMove;
            }
            boardCopy.revertLastMove();
        }

        return worstMove;
    }

    @Override
    public void updateMinMaxForActualMove(Move<Player> move) {
        boardCopy.makeMove(move);
    }

    @Override
    public void clearMinMax() {
        game = null;
        boardCopy = null;
    }

    @Override
    public String toString() {
        return "FirstMinMax";
    }
}
