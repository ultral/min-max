package implementation.simple;

import interfaces.MinMax;

public class MoveValue implements Comparable<MoveValue> {
    final MinMax.Move<MinMax.Player> move;
    int value;

    public MoveValue(final int value, final MinMax.Move<MinMax.Player> move) {
        this.value = value;
        this.move = move;
    }

    @Override
    public int compareTo(final MoveValue o) {
        final int result = value-o.value;
        if (result != 0) {
            return result;
        } else {
            return move.hashCode() - o.move.hashCode();
        }
    }

    @Override
    public String toString() {
        return move + ", value = " + value;
    }
}
