package implementation.simple;

import interfaces.MinMax;
import interfaces.MinMax.*;

import java.util.List;

import static java.lang.Integer.MIN_VALUE;

public class JustHeuristicMinMax implements MinMax<Player,
        Move<Player>,
        Board<Move<Player>>,
        Heuristic<Player, Move<Player>, Board<Move<Player>>>,
        Game<Player, Move<Player>, Board<Move<Player>>>> {

    private Game<Player, Move<Player>, Board<Move<Player>>> game;
    private Board<Move<Player>> boardCopy;

    @Override
    public void initMinMax(Game<Player, Move<Player>, Board<Move<Player>>> game) {
        this.game = game;
        this.boardCopy = game.getBoard().deepClone();
    }

    @Override
    public Move<Player> getBestMove(Player player, Heuristic<Player, Move<Player>, Board<Move<Player>>> heuristic) {
        final List<Move<Player>> allMoves = game.getAllAvailableMovesForPlayer(player);
        if (allMoves.isEmpty()) {
            throw new IllegalStateException("There are no moves on board for player: " + player.getId());
        }
        Move<Player> bestMove = null;
        int bestMoveValue = MIN_VALUE;
        int currentMoveValue;
        for (Move<Player> move : allMoves) {
            currentMoveValue = heuristic.calculateValueForMove(move, boardCopy);
            if (currentMoveValue > bestMoveValue) {
                bestMoveValue = currentMoveValue;
                bestMove = move;
            }

        }
        return bestMove;
    }

    @Override
    public void updateMinMaxForActualMove(Move<Player> move) {
        boardCopy.makeMove(move);
    }

    @Override
    public void clearMinMax() {
        game = null;
        boardCopy = null;
    }

    @Override
    public String toString() {
        return "JustHeuristic";
    }
}
