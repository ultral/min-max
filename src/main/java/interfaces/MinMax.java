package interfaces;

import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.List;

import static interfaces.MinMax.*;

public interface MinMax<
        P extends Player,
        M extends Move<P>,
        B extends Board<M>,
        H extends Heuristic<P, M, B>,
        G extends Game<P, M, B>> {

    void initMinMax(G game);

    Move<P> getBestMove(P player, H heuristic);

    void updateMinMaxForActualMove(M move);

    void clearMinMax();

    interface Game<P, M, B> {
        void initGame(List<P> players, B board);

        P getCurrentPlayer();

        List<M> getAllAvailableMovesForPlayer(P player);

        List<M> getAllAvailableMovesForPlayer(P player, B board);

        List<P> getAllPlayersImmutable();

        B getBoard();

        //if GameState.WINNER than P is winner
        SimpleImmutableEntry<GameState, P> getGameState();

        //if GameState.WINNER than P is winner
        SimpleImmutableEntry<GameState, P> getGameState(B board);

        boolean isWinningMove(M move, B board);

        void clearGame();

        enum GameState {IN_PROGRES, DRAW, WINNER}
    }

    interface Player {
        int getId();
    }

    interface Move<P> {
        P getPlayer();
    }

    interface Board<M> {
        void initMoves(List<M> moves);

        List<M> getAllMovesImmutable();

        void makeMove(M move);

        void revertLastMove();

        void clearBoard();

        Board<M> deepClone();
    }

    interface Heuristic<P, M, B> {
        int calculateValueForPlayer(P player, B board);

        int calculateValueForMove(M move, B board);

        boolean hasNeighbour(M move, B board);
    }

}
