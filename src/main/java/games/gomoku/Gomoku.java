package games.gomoku;

import implementation.simple.FirstMinMax;
import implementation.simple.JustHeuristicMinMax;
import interfaces.MinMax;
import interfaces.MinMax.*;

import java.util.*;

import static interfaces.MinMax.Game.GameState.IN_PROGRES;

public class Gomoku {

    private final List<Player> playerList = new ArrayList<>();
    private final Map<Player, Heuristic<Player, Move<Player>, Board<Move<Player>>>> playerHeuristics = new HashMap<>();
    private final Map<Player, MinMax<Player, Move<Player>, Board<Move<Player>>, Heuristic<Player, Move<Player>, Board<Move<Player>>>, Game<Player, Move<Player>, Board<Move<Player>>>>> playerMinMax = new HashMap<>();
    private GomokuBoard gomokuBoard = null;
    private GomokuGame gomokuGame = null;
    private long lastMoveFinish = 0;
    private String lastMoveDescription = "";

    public void setupGame(int playersNo, int boardSize) {
        lastMoveFinish = System.currentTimeMillis();
        playerList.clear();
        for (int i = 0; i < playersNo; i++) {
            final Player player = new GomokuPlayer(i + 1);
            playerList.add(player);
            if (i == 0) {
                playerHeuristics.put(player, new GomokuRandomMoveHeuristic());
                playerMinMax.put(player, new JustHeuristicMinMax());
            } else {
                playerHeuristics.put(player, new GomokuSimpleHeuristic());
                playerMinMax.put(player, new FirstMinMax());
            }
        }
        gomokuBoard = new GomokuBoard(boardSize);
        gomokuGame = new GomokuGame() {{
            initGame(playerList, gomokuBoard);
        }};
        playerMinMax.values().forEach(minMax -> minMax.initMinMax(gomokuGame));
    }

    public void playAutomaticMove() {
        final Player player = gomokuGame.getCurrentPlayer();
        lastMoveFinish = System.currentTimeMillis();

        playMove(playerMinMax.get(player).getBestMove(player, playerHeuristics.get(player)));

        final long moveFinish = System.currentTimeMillis() + 1;
        final long lastMoveLength = moveFinish - lastMoveFinish;
        lastMoveFinish = moveFinish;
        lastMoveDescription = "  last player - " + player.getId() +
                " (using: " + playerMinMax.get(player).toString() +
                " with: " + playerHeuristics.get(player).toString() + ") - " +
                (lastMoveLength / 1000.0) + " seconds";

    }

    public void playMove(final Move<Player> move) {
        gomokuBoard.makeMove(move);
        playerMinMax.forEach((player, minMax) -> minMax.updateMinMaxForActualMove(move));
    }

    public boolean isGameFinished() {
        return !IN_PROGRES.equals(gomokuGame.getGameState().getKey());
    }

    public String getBoardDescription() {
        final int[][] board = gomokuBoard.board;
        final int size = board.length;
        final StringBuilder sb = new StringBuilder();
        final Player currentPlayer = gomokuGame.getCurrentPlayer();
        final GomokuMove lastMove = (GomokuMove) gomokuBoard.getAllMovesImmutable().get(gomokuBoard.getAllMovesImmutable().size()-1);//ugh!
        final String boldRedOn = "\033[0;1m" + "\u001B[31m";
        final String boldOffBlack = "\033[0m" + "\u001B[0m";

        sb.append("TURN ").append(gomokuBoard.getAllMovesImmutable().size()).append(System.lineSeparator())
                .append("moving player - ").append(currentPlayer.getId())
                .append(" (using: ").append(playerMinMax.get(currentPlayer).toString())
                .append(" with: ").append(playerHeuristics.get(currentPlayer).toString())
                .append(")").append(System.lineSeparator())
                .append(lastMoveDescription).append(System.lineSeparator());
        for (int i = 0; i < size; i++) {
            sb.append((i < 9 ? " " : "")).append(i + 1).append(" : ");
            for (int j = 0; j < size; j++) {
                if (lastMove.x == j && lastMove.y == i) {
                    sb.append(boldRedOn).append(board[j][i]).append(boldOffBlack).append(' ');
                } else {
                    sb.append(board[j][i]).append(' ');
                }
            }
            sb.append(System.lineSeparator());
        }
        sb.append(System.lineSeparator()).append(System.lineSeparator());
        return sb.toString();
    }

    public void printBoard() {
        System.out.append(getBoardDescription());
    }

    public void printWinner() {
        final AbstractMap.SimpleImmutableEntry<Game.GameState, Player> gameState = gomokuGame.getGameState();
        switch (gameState.getKey()) {
            case DRAW: System.out.println("Game won by player: None"); return;
            case IN_PROGRES: System.out.println("Game in progress"); return;
            case WINNER: System.out.println("Game won by player: " + gameState.getValue().getId() + ", " + playerMinMax.get(gameState.getValue()) + ", " + playerHeuristics.get(gameState.getValue()));
        }
    }

    public GomokuGame getGomokuGame() {
        return gomokuGame;
    }
}
