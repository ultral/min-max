package games.gomoku;


import interfaces.MinMax;

public class GomokuBoardHelper {


    public static int getHorizontalForwardMax(final int steps, final int x, final int y, final int playerId, final int[][] board, final int playerIdReplace) {
        int value = 0;
        for (int i = 0; i < steps && x + i < board.length && board[x + i][y] == playerId; i++) {
            value++;
            board[x + i][y] = playerIdReplace;
        }
        return value;
    }

    public static int getVerticalDownMax(final int steps, final int x, final int y, final int playerId, final int[][] board, final int playerIdReplace) {
        int value = 0;
        for (int i = 0; i < steps && y + i < board.length && board[x][y + i] == playerId; i++) {
            value++;
            board[x][y + i] = playerIdReplace;
        }
        return value;
    }

    public static int getDiagonalDownMax(final int steps, final int x, final int y, final int playerId, final int[][] board, final int playerIdReplace) {
        int value = 0;
        for (int i = 0; i < steps && x + i < board.length && y + i < board.length && board[x + i][y + i] == playerId; i++) {
            value++;
            board[x + i][y + i] = playerIdReplace;
        }
        return value;
    }

    public static int getDiagonalUpMax(final int steps, final int x, final int y, final int playerId, final int[][] board, final int playerIdReplace) {
        int value = 0;
        for (int i = 0; i < steps && x + i < board.length && y - i > -1 && board[x + i][y - i] == playerId; i++) {
            value++;
            board[x + i][y - i] = playerIdReplace;
        }
        return value;
    }

    public static boolean neighbourExist(final GomokuMove move, final GomokuBoard board) {
        return neighbourExist(move.x - 1, move.y - 1, board.board) ||
                neighbourExist(move.x - 1, move.y, board.board) ||
                neighbourExist(move.x - 1, move.y + 1, board.board) ||
                neighbourExist(move.x, move.y - 1, board.board) ||
                neighbourExist(move.x, move.y + 1, board.board) ||
                neighbourExist(move.x + 1, move.y - 1, board.board) ||
                neighbourExist(move.x + 1, move.y, board.board) ||
                neighbourExist(move.x + 1, move.y + 1, board.board);
    }

    private static boolean neighbourExist(final int x, final int y, final int[][] board) {
        return x >= 0 && y >= 0 && x < board.length && y < board.length && board[x][y] != 0;
    }

    public static boolean fiveExists(final GomokuMove move, final GomokuBoard board) {
        return fiveExists(move, move.getPlayer(), board);
    }

    public static boolean fiveExists(final GomokuMove move, final MinMax.Player player, final GomokuBoard board) {
        return fiveExists(move.x, move.y, player.getId(), board.board);
    }

    public static boolean fiveExists(final int x, final int y, final int playerId, final int[][] board) {
        return getHorizontalMax(x, y, playerId, board) == 5 ||
                getVerticalMax(x, y, playerId, board) == 5 ||
                getDiagonalMax(x, y, playerId, board) == 5 ||
                getCounterDiagonalMax(x, y, playerId, board) == 5;
    }

    public static boolean fiveForwardExists(final int x, final int y, final int playerId, final int[][] board) {
        return checkHorizontalNum(5, x, y, playerId, board) ||
                checkVerticalNum(5, x, y, playerId, board) ||
                checkDiagonalNum(5, x, y, playerId, board) ||
                checkCounterDiagonalNum(5, x, y, playerId, board);
    }

    public static boolean checkHorizontalNum(final int num, final int x, final int y, final int playerId, final int[][] board) {
        if (x + num > board.length) return false;
        for (int i = 0; i < num; i++) {
            if (board[x + i][y] != playerId) return false;
        }
        return true;
    }

    public static boolean checkVerticalNum(final int num, final int x, final int y, final int playerId, final int[][] board) {
        if (y + num > board.length) return false;
        for (int i = 0; i < num; i++) {
            if (board[x][y + i] != playerId) return false;
        }
        return true;
    }

    public static boolean checkDiagonalNum(final int num, final int x, final int y, final int playerId, final int[][] board) {
        if (y + num > board.length || x + num > board.length) return false;
        for (int i = 0; i < num; i++) {
            if (board[x + i][y + i] != playerId) return false;
        }
        return true;
    }

    public static boolean checkCounterDiagonalNum(final int num, final int x, final int y, final int playerId, final int[][] board) {
        if (y - num < 0 || x - num < 0) return false;
        for (int i = 0; i < num; i++) {
            if (board[x - i][y - i] != playerId) return false;
        }
        return true;
    }

    public static int getHorizontalMax(int x, int y, int playerId, int[][] board) {
        if (board[x][y] != playerId) {
            return 0;
        }
        int value = 1;
        int i = 1;
        while (x + i < board.length && board[x + i][y] == playerId) {
            value++;
            i++;
        }
        i = 1;
        while (x - i >= 0 && board[x - i][y] == playerId) {
            value++;
            i++;
        }
        return Math.min(value, 5);
    }

    public static int getVerticalMax(int x, int y, int playerId, int[][] board) {
        if (board[x][y] != playerId) {
            return 0;
        }
        int value = 1;
        int i = 1;
        while (y + i < board.length && board[x][y + i] == playerId) {
            value++;
            i++;
        }
        i = 1;
        while (y - i >= 0 && board[x][y - i] == playerId) {
            value++;
            i++;
        }
        return Math.min(value, 5);
    }

    public static int getDiagonalMax(int x, int y, int playerId, int[][] board) {
        if (board[x][y] != playerId) {
            return 0;
        }
        int value = 1;
        int i = 1;
        while (x + i < board.length && y + i < board.length && board[x + i][y + i] == playerId) {
            value++;
            i++;
        }
        i = 1;
        while (x - i >= 0 && y - i >= 0 && board[x - i][y - i] == playerId) {
            value++;
            i++;
        }
        return Math.min(value, 5);
    }

    public static int getCounterDiagonalMax(int x, int y, int playerId, int[][] board) {
        if (board[x][y] != playerId) {
            return 0;
        }
        int value = 1;
        int i = 1;
        while (x - i >= 0 && y + i < board.length && board[x - i][y + i] == playerId) {
            value++;
            i++;
        }
        i = 1;
        while (x + i < board.length && y - i >= 0 && board[x + i][y - i] == playerId) {
            value++;
            i++;
        }
        return Math.min(value, 5);
    }
}
