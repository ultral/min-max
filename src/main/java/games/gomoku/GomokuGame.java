package games.gomoku;

import interfaces.MinMax.Board;
import interfaces.MinMax.Game;
import interfaces.MinMax.Move;
import interfaces.MinMax.Player;

import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static interfaces.MinMax.Game.GameState.WINNER;

public class GomokuGame implements Game<Player, Move<Player>, Board<Move<Player>>> {

    private static final SimpleImmutableEntry<GameState, Player> IN_PROGRES = new SimpleImmutableEntry<>(GameState.IN_PROGRES, null);
    private static final SimpleImmutableEntry<GameState, Player> DRAW = new SimpleImmutableEntry<>(GameState.DRAW, null);
    private List<Player> players;
    private GomokuBoard gomokuBoard;
    private SimpleImmutableEntry<GameState, Player> gameState;

    @Override
    public void initGame(List<Player> players, Board<Move<Player>> board) {
        this.players = new ArrayList<>(players);
        this.gomokuBoard = (GomokuBoard) board;
        this.gameState = IN_PROGRES;
    }

    @Override
    public Player getCurrentPlayer() {
        final int allMovesNo = gomokuBoard.getAllMovesImmutable().size();
        final int allPlayersNo = players.size();
        return players.get(allMovesNo % allPlayersNo);
    }

    @Override
    public List<Move<Player>> getAllAvailableMovesForPlayer(Player player) {
        return getAllAvailableMovesForPlayer(player, gomokuBoard);
    }

    @Override
    public List<Move<Player>> getAllAvailableMovesForPlayer(final Player player, final Board<Move<Player>> board) {
        final GomokuBoard gomokuBoard = (GomokuBoard) board;
        final LinkedList<Move<Player>> availableMoves = new LinkedList<>();
        for (int i = 0; i < gomokuBoard.size; i++) {
            for (int j = 0; j < gomokuBoard.size; j++) {
                if (gomokuBoard.board[i][j] == 0) {
                    availableMoves.addLast(new GomokuMove(i, j, player));
                }
            }
        }
        return availableMoves;
    }

    @Override
    public List<Player> getAllPlayersImmutable() {
        return Collections.unmodifiableList(players);
    }

    @Override
    public GomokuBoard getBoard() {
        return gomokuBoard;
    }

    @Override
    public void clearGame() {
        players.clear();
        gomokuBoard.clearBoard();
        gameState = IN_PROGRES;
    }

    @Override
    public SimpleImmutableEntry<GameState, Player> getGameState() {
        this.gameState = getGameState(gomokuBoard);
        return gameState;
    }

    @Override
    public SimpleImmutableEntry<GameState, Player> getGameState(Board<Move<Player>> board) {
        for (int i = 0; i < gomokuBoard.size; i++) {
            for (int j = 0; j < gomokuBoard.size; j++) {
                if (gomokuBoard.board[i][j] != 0) {
                    if (GomokuBoardHelper.fiveExists(i, j, gomokuBoard.board[i][j], gomokuBoard.board)) {
                        final int winnerId = gomokuBoard.board[i][j];
                        final Player winner = players.stream().filter(p -> p.getId() == winnerId).findFirst().orElse(null);
                        return new SimpleImmutableEntry<>(WINNER, winner);
                    }
                }
            }
        }

        for (int i = 0; i < gomokuBoard.size; i++) {
            for (int j = 0; j < gomokuBoard.size; j++) {
                if (gomokuBoard.board[i][j] == 0) { //empty space there is still move possibility
                    return IN_PROGRES;
                }
            }
        }
        //NO empty space NO move possibility
        return DRAW;
    }

    @Override
    public boolean isWinningMove(final Move<Player> move, final Board<Move<Player>> board) {
        board.makeMove(move);
        final GomokuMove gomokuMove = (GomokuMove) move;
        final GomokuBoard gomokuBoard = (GomokuBoard) board;
        final boolean moveOutcome = GomokuBoardHelper.fiveExists(gomokuMove.x, gomokuMove.y, gomokuMove.getPlayer().getId(), gomokuBoard.board);
        board.revertLastMove();
        return moveOutcome;
    }
}
