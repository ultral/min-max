package games.gomoku;

import interfaces.MinMax.Move;
import interfaces.MinMax.Player;

import java.util.Objects;

public class GomokuMove implements Move<Player> {
    public int x;
    public int y;
    Player player;

    public GomokuMove(int x, int y, Player player) {
        this.x = x;
        this.y = y;
        this.player = player;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    @Override
    public String toString() {
        return "x=" + x + ", y=" + y + ", " + player ;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final GomokuMove that = (GomokuMove) o;
        return x == that.x && y == that.y && Objects.equals(getPlayer(), that.getPlayer());
    }

}
