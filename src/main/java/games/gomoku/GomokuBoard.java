package games.gomoku;

import interfaces.MinMax.Board;
import interfaces.MinMax.Move;
import interfaces.MinMax.Player;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static java.util.Collections.unmodifiableList;

public class GomokuBoard implements Board<Move<Player>> {

    public final int[][] board;
    final int size;
    private final LinkedList<GomokuMove> moves;

    public GomokuBoard(int size) {
        if (size < 5) {
            throw new IllegalArgumentException("Gomoku board size must be greater than 5.");
        }
        this.size = size;
        this.board = new int[size][size];
        this.moves = new LinkedList<>();
    }

    @Override
    public void initMoves(List<Move<Player>> moves) {
        for (Move<Player> move : moves) {
            makeMove(move);
        }
    }

    @Override
    public List<Move<Player>> getAllMovesImmutable() {
        return unmodifiableList(moves);
    }

    @Override
    public void makeMove(Move<Player> move) {
        if (move == null) {
            throw new IllegalArgumentException("Move cannot be null!");
        }
        //todo he we use gomoku move
        final GomokuMove gm = (GomokuMove) move;
        validateMove(gm.x, gm.y);
        board[gm.x][gm.y] = gm.getPlayer().getId();
        moves.addLast(gm);
    }

    void validateMove(final int x, final int y) {
        if (x < 0 || x >= size || y < 0 || y >= size) {
            throw new IllegalArgumentException("Move coordinates are not inside board. X = " + x + ", Y = " + y + ", size = " + size);
        }
        if (board[x][y] != 0) {
            throw new IllegalArgumentException("Board on coordinates X = " + x + ", Y = " + y + " are already taken by player: " + board[x][y]);
        }
    }

    @Override
    public void revertLastMove() {
        final GomokuMove move = moves.removeLast();
        board[move.x][move.y] = 0;
    }

    @Override
    public void clearBoard() {
        moves.clear();
        for (int i = 0; i < size; i++) {
            Arrays.fill(board[i], 0);
        }
    }

    @Override
    public Board<Move<Player>> deepClone() {
        final GomokuBoard clone = new GomokuBoard(this.size);
        clone.initMoves(this.getAllMovesImmutable());
        return clone;
    }
}
