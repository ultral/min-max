package games.gomoku;

import interfaces.MinMax.Board;
import interfaces.MinMax.Heuristic;
import interfaces.MinMax.Move;

import static games.gomoku.GomokuBoardHelper.*;
import static interfaces.MinMax.Player;

public class GomokuSimpleHeuristic implements Heuristic<Player, Move<Player>, Board<Move<Player>>> {

    private final int[] heuristic_tab = new int[6];

    public GomokuSimpleHeuristic() {
        setHeuristic(0, 0);
        setHeuristic(1, 10);
        setHeuristic(2, 100);
        setHeuristic(3, 1000);
        setHeuristic(4, 10000);
        setHeuristic(5, 10000000);
    }

    private static int distanceFromCenter(final int x, final int y, final int boardLength) {
        final int middle = boardLength / 2;
        return Math.abs(x - middle) + Math.abs(y - middle);
    }

    private static void cleanupBoardTab(final int playerId, final int playerIdReplace, final int[][] board) {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {
                if (board[i][j] == playerIdReplace) {
                    board[i][j] = playerId;
                }
            }
        }
    }

    private static int horizontalHeuristic(final int playerId, final int[][] board, final int[] heuristic) {
        int value = 0;
        final int playerIdReplace = -playerId;
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {
                if (board[i][j] == playerId) {
                    value += heuristic[getHorizontalForwardMax(5, i, j, playerId, board, playerIdReplace)];
                }
            }
        }
        cleanupBoardTab(playerId, playerIdReplace, board);
        return value;
    }

    private static int verticalHeuristic(final int playerId, final int[][] board, final int[] heuristic) {
        int value = 0;
        final int playerIdReplace = -playerId;
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {
                if (board[i][j] == playerId) {
                    value += heuristic[getVerticalDownMax(5, i, j, playerId, board, playerIdReplace)];
                }
            }
        }
        cleanupBoardTab(playerId, playerIdReplace, board);
        return value;
    }

    private static int diagonalDownHeuristic(final int playerId, final int[][] board, final int[] heuristic) {
        int value = 0;
        final int playerIdReplace = -playerId;
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {
                if (board[i][j] == playerId) {
                    value += heuristic[getDiagonalDownMax(5, i, j, playerId, board, playerIdReplace)];
                }
            }
        }
        cleanupBoardTab(playerId, playerIdReplace, board);
        return value;
    }

    private static int diagonalUpHeuristic(final int playerId, final int[][] board, final int[] heuristic) {
        int value = 0;
        final int playerIdReplace = -playerId;
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {
                if (board[i][j] == playerId) {
                    value += heuristic[getDiagonalUpMax(5, i, j, playerId, board, playerIdReplace)];
                }
            }
        }
        cleanupBoardTab(playerId, playerIdReplace, board);
        return value;
    }

    public void setHeuristic(final int count, final int heuristicValue) {
        heuristic_tab[count] = heuristicValue;
    }

    @Override
    public int calculateValueForMove(final Move<Player> move, final Board<Move<Player>> board) {
        final GomokuBoard gomokuBoard = (GomokuBoard) board;
        final GomokuMove gomokuMove = (GomokuMove) move;
        int value = 0;
        board.makeMove(move);
        value += heuristic_tab[getHorizontalMax(gomokuMove.x, gomokuMove.y, move.getPlayer().getId(), gomokuBoard.board)];
        value += heuristic_tab[getVerticalMax(gomokuMove.x, gomokuMove.y, move.getPlayer().getId(), gomokuBoard.board)];
        value += heuristic_tab[getDiagonalMax(gomokuMove.x, gomokuMove.y, move.getPlayer().getId(), gomokuBoard.board)];
        value += heuristic_tab[getCounterDiagonalMax(gomokuMove.x, gomokuMove.y, move.getPlayer().getId(), gomokuBoard.board)];
        value -= distanceFromCenter(gomokuMove.x, gomokuMove.y, gomokuBoard.board.length);

        board.revertLastMove();
        return value;
    }

    @Override
    public int calculateValueForPlayer(final Player player, final Board<Move<Player>> board) {
        final GomokuBoard gomokuBoard = (GomokuBoard) board;
        final int[][] board_tab = gomokuBoard.board;
        final int playerId = player.getId();
        int value = 0;

        value += horizontalHeuristic(playerId, board_tab, heuristic_tab);
        value += verticalHeuristic(playerId, board_tab, heuristic_tab);
        value += diagonalDownHeuristic(playerId, board_tab, heuristic_tab);
        value += diagonalUpHeuristic(playerId, board_tab, heuristic_tab);

        return value;
    }

    @Override
    public boolean hasNeighbour(final Move<Player> move, final Board<Move<Player>> board) {
        return GomokuBoardHelper.neighbourExist((GomokuMove) move, (GomokuBoard) board);
    }

    @Override
    public String toString() {
        return "SimpleHeuristic";
    }
}
