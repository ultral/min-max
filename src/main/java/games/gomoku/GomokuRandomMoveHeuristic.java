package games.gomoku;

import interfaces.MinMax.Board;
import interfaces.MinMax.Heuristic;
import interfaces.MinMax.Move;

import java.util.Random;

import static interfaces.MinMax.Player;

public class GomokuRandomMoveHeuristic implements Heuristic<Player, Move<Player>, Board<Move<Player>>> {

    private final Random random = new Random(System.currentTimeMillis());

    @Override
    public int calculateValueForPlayer(final Player player, final Board<Move<Player>> board) {
        return random.nextInt();
    }

    @Override
    public int calculateValueForMove(Move<Player> move, Board<Move<Player>> board) {
        return random.nextInt();
    }

    @Override
    public boolean hasNeighbour(final Move<Player> move, final Board<Move<Player>> board) {
        return GomokuBoardHelper.neighbourExist((GomokuMove) move, (GomokuBoard) board);
    }

    @Override
    public String toString() {
        return "RandomMoveHeuristic";
    }
}
