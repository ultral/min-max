package games.gomoku;

import interfaces.MinMax;

public class GomokuPlayer implements MinMax.Player {
    private final int id;

    public GomokuPlayer(int id) {
        this.id = id;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "id=" + id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final GomokuPlayer that = (GomokuPlayer) o;
        return id == that.id;
    }
}
