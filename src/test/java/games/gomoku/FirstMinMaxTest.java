package games.gomoku;

import interfaces.MinMax.Move;
import interfaces.MinMax.Player;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.in;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.*;

public class FirstMinMaxTest {

    private static void playMove(final Gomoku game, final Player player, int x, int y) {
        game.playMove(new GomokuMove(x, y, player));
    }

    private static String getBoardDescription(final String message, final Gomoku gomoku) {
        return message + System.lineSeparator() + gomoku.getBoardDescription();
    }

    private static GomokuMove lastMove(final Gomoku game) {
        final List<Move<Player>> allMoves = game.getGomokuGame().getBoard().getAllMovesImmutable();
        return (GomokuMove) allMoves.get(allMoves.size() - 1);
    }

    @Test
    void fiveExistsTest() {
        final Gomoku game = new Gomoku() {{
            setupGame(2, 7);
        }};

        final Player pl1 = game.getGomokuGame().getAllPlayersImmutable().stream().filter(it -> it.getId() == 1).findFirst().get();
        final Player pl2 = game.getGomokuGame().getAllPlayersImmutable().stream().filter(it -> it.getId() == 2).findFirst().get();

        playMove(game, pl1, 3, 3);
        playMove(game, pl2, 0, 0);
        playMove(game, pl1, 2, 3);
        playMove(game, pl2, 6, 0);
        playMove(game, pl1, 4, 3);
        playMove(game, pl2, 1, 3);
        playMove(game, pl1, 5, 3);
        playMove(game, pl1, 6, 3);


        assertTrue(GomokuBoardHelper.fiveExists(lastMove(game), game.getGomokuGame().getBoard()),
                getBoardDescription("Five Exist!", game));
    }

    @Test
    void fiveVerticalTest() {
        final Gomoku game = new Gomoku() {{
            setupGame(2, 7);
        }};

        final Player pl1 = game.getGomokuGame().getAllPlayersImmutable().stream().filter(it -> it.getId() == 1).findFirst().get();

        playMove(game, pl1, 0, 0);
        playMove(game, pl1, 0, 1);
        playMove(game, pl1, 0, 2);
        playMove(game, pl1, 0, 3);
        playMove(game, pl1, 0, 4);
        assertTrue(GomokuBoardHelper.fiveExists(lastMove(game), game.getGomokuGame().getBoard()),
                getBoardDescription("Five vertical Exist!", game));
    }

    @Test
    void fiveHorizontalTest() {
        final Gomoku game = new Gomoku() {{
            setupGame(2, 7);
        }};

        final Player pl1 = game.getGomokuGame().getAllPlayersImmutable().stream().filter(it -> it.getId() == 1).findFirst().get();

        playMove(game, pl1, 0, 0);
        playMove(game, pl1, 1, 0);
        playMove(game, pl1, 2, 0);
        playMove(game, pl1, 3, 0);
        playMove(game, pl1, 4, 0);
        assertTrue(GomokuBoardHelper.fiveExists(lastMove(game), game.getGomokuGame().getBoard()),
                getBoardDescription("Five horizontal Exist!", game));
    }

    @Test
    void fiveDiagonalTest() {
        final Gomoku game = new Gomoku() {{
            setupGame(2, 7);
        }};

        final Player pl1 = game.getGomokuGame().getAllPlayersImmutable().stream().filter(it -> it.getId() == 1).findFirst().get();

        playMove(game, pl1, 0, 0);
        playMove(game, pl1, 1, 1);
        playMove(game, pl1, 2, 2);
        playMove(game, pl1, 3, 3);
        playMove(game, pl1, 4, 4);
        assertTrue(GomokuBoardHelper.fiveExists(lastMove(game), game.getGomokuGame().getBoard()),
                getBoardDescription("Five diagonal Exist!", game));
    }

    @Test
    void fiveCounterDiagonalTest() {
        final Gomoku game = new Gomoku() {{
            setupGame(2, 7);
        }};

        final Player pl1 = game.getGomokuGame().getAllPlayersImmutable().stream().filter(it -> it.getId() == 1).findFirst().get();

        playMove(game, pl1, 0, 4);
        playMove(game, pl1, 1, 3);
        playMove(game, pl1, 2, 2);
        playMove(game, pl1, 3, 1);
        playMove(game, pl1, 4, 0);
        assertTrue(GomokuBoardHelper.fiveExists(lastMove(game), game.getGomokuGame().getBoard()),
                getBoardDescription("Five counter diagonal Exist!", game));
    }

    @Test
    void playAutomaticMoveTest() {
        final Gomoku game = new Gomoku() {{
            setupGame(2, 7);
        }};

        final Player pl1 = game.getGomokuGame().getAllPlayersImmutable().stream().filter(it -> it.getId() == 1).findFirst().get();
        final Player pl2 = game.getGomokuGame().getAllPlayersImmutable().stream().filter(it -> it.getId() == 2).findFirst().get();

        playMove(game, pl1, 3, 3);
        playMove(game, pl2, 0, 0);
        playMove(game, pl1, 2, 3);
        playMove(game, pl2, 6, 0);
        playMove(game, pl1, 4, 3);
        playMove(game, pl2, 1, 3);
        playMove(game, pl1, 5, 3);

        game.playAutomaticMove();

        assertEquals(new GomokuMove(6, 3, pl2),
                lastMove(game),
                getBoardDescription("Wrong automatic move!", game));
    }

    @Test
    void playAutomaticMove2Test() {
        final Gomoku game = new Gomoku() {{
            setupGame(2, 7);
        }};

        final Player pl1 = game.getGomokuGame().getAllPlayersImmutable().stream().filter(it -> it.getId() == 1).findFirst().get();
        final Player pl2 = game.getGomokuGame().getAllPlayersImmutable().stream().filter(it -> it.getId() == 2).findFirst().get();

        playMove(game, pl1, 0, 0);
        playMove(game, pl2, 2, 2);
        playMove(game, pl1, 6, 0);
        playMove(game, pl2, 2, 3);
        playMove(game, pl1, 0, 6);
        playMove(game, pl2, 2, 4);
        playMove(game, pl1, 0, 3);
        playMove(game, pl2, 3, 4);
        playMove(game, pl1, 6, 3);
        playMove(game, pl2, 4, 4);
        playMove(game, pl1, 6, 6);
        game.playAutomaticMove();

        assertThat(getBoardDescription("Wrong automatic move!", game),
                lastMove(game),
                is(in(List.of(new GomokuMove(1, 4, pl2), new GomokuMove(2, 5, pl2))))
        );
    }

    @Test
    void playAutomaticHorizontalMoveTest() {
        final Gomoku game = new Gomoku() {{
            setupGame(2, 7);
        }};

        final Player pl1 = game.getGomokuGame().getAllPlayersImmutable().stream().filter(it -> it.getId() == 1).findFirst().get();
        final Player pl2 = game.getGomokuGame().getAllPlayersImmutable().stream().filter(it -> it.getId() == 2).findFirst().get();

        playMove(game, pl1, 0, 0);
        playMove(game, pl2, 2, 3);
        playMove(game, pl1, 0, 5);
        playMove(game, pl2, 4, 3);
        playMove(game, pl1, 6, 0);
        playMove(game, pl2, 3, 3);
        playMove(game, pl1, 6, 1);
        game.playAutomaticMove();

        assertThat(getBoardDescription("Wrong horizontal automatic move!", game),
                lastMove(game),
                is(in(List.of(new GomokuMove(5, 3, pl2), new GomokuMove(1, 3, pl2))))
        );
    }

    @Test
    void playAutomaticVerticalMoveTest() {
        final Gomoku game = new Gomoku() {{
            setupGame(2, 7);
        }};

        final Player pl1 = game.getGomokuGame().getAllPlayersImmutable().stream().filter(it -> it.getId() == 1).findFirst().get();
        final Player pl2 = game.getGomokuGame().getAllPlayersImmutable().stream().filter(it -> it.getId() == 2).findFirst().get();

        playMove(game, pl1, 0, 0);
        playMove(game, pl2, 3, 2);
        playMove(game, pl1, 0, 5);
        playMove(game, pl2, 3, 4);
        playMove(game, pl1, 6, 0);
        playMove(game, pl2, 3, 3);
        playMove(game, pl1, 6, 1);
        game.playAutomaticMove();

        assertThat(getBoardDescription("Wrong vertical automatic move!", game),
                lastMove(game),
                is(in(List.of(new GomokuMove(3, 5, pl2), new GomokuMove(3, 1, pl2))))
        );
    }

    @Test
    void playAutomaticDiagonalMoveTest() {
        final Gomoku game = new Gomoku() {{
            setupGame(2, 7);
        }};

        final Player pl1 = game.getGomokuGame().getAllPlayersImmutable().stream().filter(it -> it.getId() == 1).findFirst().get();
        final Player pl2 = game.getGomokuGame().getAllPlayersImmutable().stream().filter(it -> it.getId() == 2).findFirst().get();

        playMove(game, pl1, 0, 0);
        playMove(game, pl2, 2, 2);
        playMove(game, pl1, 0, 5);
        playMove(game, pl2, 3, 3);
        playMove(game, pl1, 6, 0);
        playMove(game, pl2, 4, 4);
        playMove(game, pl1, 6, 1);
        game.playAutomaticMove();

        assertEquals(new GomokuMove(5, 5, pl2),
                lastMove(game),
                getBoardDescription("Wrong Diagonal automatic move!", game));
    }

    @Test
    void playAutomaticCounterDiagonalMoveTest() {
        final Gomoku game = new Gomoku() {{
            setupGame(2, 7);
        }};

        final Player pl1 = game.getGomokuGame().getAllPlayersImmutable().stream().filter(it -> it.getId() == 1).findFirst().get();
        final Player pl2 = game.getGomokuGame().getAllPlayersImmutable().stream().filter(it -> it.getId() == 2).findFirst().get();

        playMove(game, pl1, 0, 0);
        playMove(game, pl2, 4, 2);
        playMove(game, pl1, 6, 6);
        playMove(game, pl2, 3, 3);
        playMove(game, pl1, 6, 0);
        playMove(game, pl2, 2, 4);
        playMove(game, pl1, 6, 1);
        game.playAutomaticMove();

        assertEquals(new GomokuMove(1, 5, pl2),
                lastMove(game),
                getBoardDescription("Wrong Counter Diagonal automatic move!", game));
    }

    @Test
    void fiveNotExistsDiagonalTest() {
        final Gomoku game = new Gomoku() {{
            setupGame(2, 7);
        }};

        final Player pl1 = game.getGomokuGame().getAllPlayersImmutable().stream().filter(it -> it.getId() == 1).findFirst().get();
        final Player pl2 = game.getGomokuGame().getAllPlayersImmutable().stream().filter(it -> it.getId() == 2).findFirst().get();

        playMove(game, pl1, 3, 3);
        playMove(game, pl2, 0, 1);
        playMove(game, pl2, 1, 1);
        playMove(game, pl2, 2, 1);
        playMove(game, pl2, 3, 1);
        playMove(game, pl2, 3, 2);
        playMove(game, pl2, 4, 2);
        playMove(game, pl2, 4, 3);
        playMove(game, pl2, 1, 0);

        assertFalse(GomokuBoardHelper.fiveExists(lastMove(game), game.getGomokuGame().getBoard()),
                getBoardDescription("Five do not Exist!", game));
    }

    @Test
    void fiveNotExistsCounterDiagonalTest() {
        final Gomoku game = new Gomoku() {{
            setupGame(2, 7);
        }};

        final Player pl1 = game.getGomokuGame().getAllPlayersImmutable().stream().filter(it -> it.getId() == 1).findFirst().get();
        final Player pl2 = game.getGomokuGame().getAllPlayersImmutable().stream().filter(it -> it.getId() == 2).findFirst().get();

        playMove(game, pl1, 0, 0);
        playMove(game, pl2, 1, 1);
        playMove(game, pl2, 2, 2);
        playMove(game, pl2, 3, 3);
        playMove(game, pl2, 5, 3);
        playMove(game, pl2, 4, 4);

        assertFalse(GomokuBoardHelper.fiveExists(lastMove(game), game.getGomokuGame().getBoard()),
                getBoardDescription("Five do not Exist!", game));
    }

}
