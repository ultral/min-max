### Implementation of [min-max](https://en.wikipedia.org/wiki/Minimax) algorythm in [Gomoku](https://en.wikipedia.org/wiki/Gomoku)

Link gitlab group [link](https://gitlab.com/ultral/min-max).

#### Required tools

- Java development kit 8 [link](https://www.oracle.com/java/technologies/downloads/#java8)
- Apache maven 3.6.+ [link](https://maven.apache.org/download.cgi)
- Git [link](https://git-scm.com/downloads)

download and install required tools.

#### Building instruction

- Download app source code: [link](https://gitlab.com/ultral/min-max)
- Run maven: mvn clean install
- Run maven for tests: mvn clean install -Pall-tests